const express = require('express')

const app = express()
const port = process.env.PORT || 3000
app.set('views', './views')
app.set('view engine', 'pug')


app.get('/', (req, res) => {
  res.render('index', { message: 'Hello World from Gitlab! Heres something a bit more interesting' })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
